/**
 * Created by Pieter-Jan
 */
function clock(object) {
    if (typeof object.tagName !== 'undefined' && object.tagName.toUpperCase() === "OBJECT") {
        var obj = {
            clock: object,
            seconds_hand: null,
            minutes_hand: null,
            hours_hand: null,
            seconds: null,
            minutes: null,
            hours: null,
            second_angle: null,
            minute_angle: null,
            hour_angle: null,
            ticker: null,
            init: function(hours, minutes, seconds, tick) {
                var that = this;

                this.second_angle = typeof seconds === 'undefined' ? this.clock.getAttribute("data-secondAngle") : seconds % 60 * 6;
                this.minute_angle = typeof seconds === 'undefined' ? this.clock.getAttribute("data-minuteAngle") : minutes % 60 * 6;
                this.hour_angle = typeof seconds === 'undefined' ? this.clock.getAttribute("data-hourAngle") : hours % 12 * 30 + (this.minute_angle / 12);

                this.seconds = this.second_angle / 6;
                this.minutes = this.minute_angle / 6;
                this.hours = (this.hour_angle - this.minute_angle / 12) / 30;

                this.clock.addEventListener("load", function() {
                    var content = that.clock.contentDocument;
                    that.seconds_hand = content.querySelector(".seconds_hand");
                    that.minutes_hand = content.querySelector(".minutes_hand");
                    that.hours_hand = content.querySelector(".hours_hand");

                    if (that.clock.getAttribute("data-tick") === "true" || typeof tick !== 'undefined')
                        that.tick(that.second_angle);
                    else if (seconds == false) {
                        content.querySelector(".seconds_hand").style.display = 'none';

                    }

                    that.rotateHands([that.seconds_hand, that.minutes_hand, that.hours_hand], [that.second_angle, that.minute_angle, that.hour_angle]);

                }, false);



                return obj;
            },
            rotateHands: function(hands, deg) {
                for (var a = 0; a < hands.length; a++) {
                    if (hands[a] != null) {
                        hands[a].style.webkitTransform = 'rotate(' + deg[a] + 'deg)';
                        hands[a].style.mozTransform = 'rotate(' + deg[a] + 'deg)';
                        hands[a].style.msTransform = 'rotate(' + deg[a] + 'deg)';
                        hands[a].style.oTransform = 'rotate(' + deg[a] + 'deg)';
                        hands[a].style.transform = 'rotate(' + deg[a] + 'deg)';
                    }
                }
            },
            tick: function(start) {
                var that = this;
                if (start === 'false') {
                    clearInterval(that.ticker);
                    that.seconds = null;
                } else {
                    that.ticker = setInterval(function() {
                        that.seconds++;
                        var deg = that.seconds % 60 * 6;
                        that.rotateHands([that.clock.contentDocument.querySelector(".seconds_hand")], [deg]);
                    }, 1000);
                }
            },
            setHours: function(hours) {
                this.hours = hours;
                this.hour_angle = ((this.hours % 12) * 30) + (this.minute_angle / 12);
                this.rotateHands([this.clock.contentDocument.querySelector(".hours_hand")], [this.hour_angle]);
            },
            setMinutes: function(minutes) {
                this.minutes = minutes;
                this.minute_angle = minutes % 60 * 6;
                this.hour_angle = ((this.hours % 12) * 30) + (this.minute_angle / 12);
                this.rotateHands([this.clock.contentDocument.querySelector(".minutes_hand"),this.clock.contentDocument.querySelector(".hours_hand")], [this.minute_angle, this.hour_angle]);
            },
            setSeconds: function(seconds) {
                this.seconds = seconds;
                this.second_angle = seconds % 60 * 6;
                this.rotateHands([this.clock.contentDocument.querySelector(".seconds_hand")], [this.second_angle]);
            },
            setTime: function(hours,minutes,seconds){
                this.setHours(hours);
                this.setMinutes(minutes);
                this.setSeconds(seconds);
            },
            getHours: function() {
                return this.hours;
            },
            getMinutes: function() {
                return this.minutes;
            },
            getSeconds: function() {
                return this.seconds;
            },
            getTime: function() {
                return [this.hours,this.minutes, this.seconds];
            }
        };
        return obj;
    }
    return 'error';
}