<?php

namespace RikaTika\User\AllBundle\Twig;

class ClocklearningExtension extends \Twig_Extension
{

    protected $router;

    public function __construct($router)
    {
        $this->router = $router;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('analogClock', array($this, 'generateAnalogClock'), array('is_safe' => array('html'))),
        );
    }

    function generateAnalogClock($id = "analogClock" , $time = [0,0,0] , $ticker = false) {
        $context = $this->router->getContext();
        $host = $context->getScheme().'://'.$context->getHost();

        $secondAngle = 0;
        $minuteAngle = 0;
        $hourAngle = 0;

        foreach($time as $i=>$t) {
            switch ($i) {
                case 2:
                    $secondAngle = $t % 60 * 6;
                break;
                case 1:
                    $minuteAngle = $t % 60 * 6;
                    $hourAngle += $minuteAngle / 12;

                break;
                case 0:
                    $hourAngle = $t % 12 * 30;
                break;
            }
        }

        if ($ticker != false) {
            $ticker = true;
        }
        return '<object data="'. $host .'/clock/svg/analog.svg'.'" id="'. $id .'" data-tick="'. $ticker . '" data-secondAngle="' . $secondAngle . '" data-minuteAngle="' . $minuteAngle . '" data-hourAngle="' . $hourAngle . '"  type="image/svg+xml"></object>';
    }
    public function getName() {
        return 'clockgenerator';
    }
}