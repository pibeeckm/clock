analogClock
-----------

    {{ analogClock ( id, time, tick ) }}

Returns an svg object of an analog clock

##Instructions
- Place *ClocklearningExtension.php* in www/src/RikaTika/User/AllBundle/Twig (*)
- Register the service in your *services.yml* file located in www/src/RikaTika/User/AllBundle/Resources/config/services.yml
	Add the following code to the file:
```
services:
  rikatika.user.allbundle.twig.clockgenerator:
      class: RikaTika\User\AllBundle\Twig\ClocklearningExtension
      arguments:  [@router]
      tags:
          - { name: twig.extension }
```
- place the clock folder in your web folder
- use twig function **{{ analogClock(id, time, tick) }}** to generate clock


##Parameters

| Parameter        | Type           | Description                  |
| ------------- |--------------- | -----------------------------|
| `id`       | string (default: "analogClock")            | Hours to display, 24h format (Required) |
| `time`    | array(h,m,s) (default: "[0,0,0]")            | Minutes to display (Required)      |
| `tick`     | bool (default: false)                              |make clock tick                |


##Usage

Place the clock folder in your public web folder (this includes the js svg file)
next you have to link the JS file
the following code will create a default analog clock

```
{{ analogClock ("clockId") }}
```

Before the correct time is shown, you have to init your clock object, this can be done the following way:
```
var object = clock(document.querySelector("#clockId"));
object.init();
```
or
```
var object = clock(document.querySelector("#clockId"));
object.init(23,5,24);
```
You can then use the following functions:
```
var object = clock(document.querySelector("#clockId"));
object.init(hours, minutes, seconds, tick);

object.setHours(hours);
object.setMinutes(minutes);
object.setSeconds(seconds);
object.setTime(hours, minutes, seconds);

object.getHours();
object.getMinutes();
object.getSeconds();
object.getTime();

object.tick("true"); //Let the clock tick
object.tick("false"); //Don't let the clock tick


```


----------
(*) I Have included the empty folder structure (including ClocklearningExtension.php) in the zip so you can just paste and merge the folders and file